from ProjetGestionEtudiants import Cours
from ProjetGestionEtudiants import Etudiant

# Initialisation
etudiants = [
    Etudiant("Dupont", "Gilles", "DupontG", "Dupontpass"),
    Etudiant("Doe", "John", "DoeJ", "Doepass"),
    Etudiant("Smith", "Jane", "SmithJ", "Smithpass"),
    Etudiant("Yagami", "Light", "YagamiL", "Yagamipass")
]
etudiants[0].ajoutNote(13)
etudiants[0].ajoutNote(12)
etudiants[1].ajoutNote(16)
etudiants[1].ajoutNote(14)
etudiants[2].ajoutNote(12)
etudiants[2].ajoutNote(16)
etudiants[3].ajoutNote(19)
etudiants[3].ajoutNote(16)

cours = [
    Cours("biologie"),
    Cours("ethologie")    
]
cours[0].ajoutEtud(etudiants[0])
cours[0].ajoutEtud(etudiants[2])
cours[1].ajoutEtud(etudiants[1])
cours[1].ajoutEtud(etudiants[3])


options = [
    "Afficher tous les etudiants et/ou tous les cours",
    "Ajouter un etudiant",
    "Creer un nouveau cours",
    "Afficher la liste des etudiants inscrit a un cours",
    "Assigner des notes a un etudiant",
    "Creer un utilisateur",
    "Visualiser les notes par cours",
       
]

#Fonctions & variables
optEnd = "\n*******************************"

optErr = "Commande inconnue."

numErr = "Nombre invalide"

def menu():
    menu = "\n"
    for i in range(0,6):
        menu += str(i) + ". " + options[i].capitalize() + "\n"
    print menu

def menuetu():
    menuetu = "\n"
    for i in range (6,7):
        menuetu += str(i) + ". " + options[i].capitalize() + "\n"
    print menuetu

def menuprof():
    menuprof = "\n"
    for i in range (2,5):
        menuprof += str(i) + ". " + options[i].capitalize() + "\n"
    print menuprof
    
def optTitle(n):
    if n in range(0, len(options)):
        thisOpt = options[n]
        d = "de "
        if thisOpt[0].lower() in ["a", "e", "y", "u", "i", "o"]:
            d = "d'"
        print "\nVous avez choisi " + d + options[n] + "."
    elif n != -1:
        print "\n" + optErr

def validNumInput(num, List, error):
    if num.isdigit(): 
        num = int(num)-1
        if num in range(0, len(List)):
            return True
        else:
            print error
            return False
    else:
        print error
        return False

def validNote(num, min, max, error):
    if num.isdigit(): 
        num = int(num)
        if num >=min and num <=max:
            return True
        else:
            print error
            return False
    else:
        print error
        return False

def getCoursList():
    print "Selectionnez un cours parmi ceux disponibles:"
    for i in range(0, len(cours)):            
        print str(i + 1) + ". " + str(cours[i].nom)  
    
def getCours(p):
    if p.isdigit(): 
        p = int(p) - 1
        if p in range(0,len(cours)):
            return  cours[p]
        else:
            print optErr
            return False
    else:
        print optErr
        return False  
 

# Application "Moodle"
name = raw_input("entrez votre identifiant:")
pwd = raw_input("entrez votre mot de passe:")
if name =='admin' and pwd =='adminpass':
    print("Bienvenue dans l'application \"Moodle\"!")          
    while True:
        menu()
         
    
        choix = raw_input("Votre choix: ")
        if choix.lstrip('-').isdigit():
            choix = int(choix)
            optTitle(choix)

            if choix == 0:
                print "\nChoisissez le contenu a afficher:"
                print "1. Tous les etudiants"
                print "2. Tous les cours"
                print "3. Tous les etudiants et tous les cours"
                n = raw_input("\nVotre choix: ")
                if n.isdigit(): 
                    n = int(n)
                    if  n in range(1,4):
                        if n == 1 or n == 3:
                            print ""
                            for etud in etudiants:
                                print str(etud)
                        if n == 2 or n == 3:
                            print ""
                            for matiere in  cours:
                                print str(matiere)
                                if matiere != cours[-1]:
                                    print ""
                    else:
                        print optErr
                else:
                    print optErr             

            if choix == 1:
                nom = raw_input("Nom: ")
                prenom = raw_input("Prenom: ")
                ident = raw_input("Identifiant: ")
                mdp = raw_input("Mot de passe: ")
                if ident.isalpha():       
                    etu = Etudiant(nom, prenom, ident, mdp)
                    print "\nEntrez les notes, -1 pour terminer"
                    n = raw_input("Note: ")
                    while n != "-1":
                        if validNote(n, 0, 20, numErr):
                                etu.ajoutNote(int(n))
                        n = raw_input("Note: ")
                    etudiants.append(etu)
                    print "\nNouvel Etudiant : " + str(etu)
                else:
                    print optErr


            if choix == 2:
                nom = raw_input("Nom: ")
                matiere = Cours(nom)
                print "\nSelectionnez les etudiants, -1 pour terminer"
                for i in range(0, len(etudiants)):            
                    print str(i + 1) + ". " + str(etudiants[i])
                n = raw_input("\nVotre choix: ")
                while n != "-1":
                    if validNumInput(n, etudiants, optErr):
                        n = int(n)-1
                        if etudiants[n] not in matiere.etudiants:
                            matiere.ajoutEtud(etudiants[n])
                        else:
                            print "Deja present dans le cours"
                    n = raw_input("Votre choix: ")
                cours.append(matiere)
                print "\nNouveau Cours : " + str(matiere)

            if choix == 3:
                getCoursList()   
                n = raw_input("\nVotre choix: ")
                if validNumInput(n,  cours, optErr):
                    print "\n"+str(cours[int(n)-1])  
                
            if choix == 4:
                s = raw_input("Entrez le nom de l'etudiant: ")
                s = s.upper()
                etudList = []
                for etud in etudiants:
                    if s == etud.nom.upper():
                        etudList.append(etud)        
                if len(etudList) > 0:
                    if len(etudList) > 1:
                        print "\nSelectionnez l'etudiant:"
                        for i in range(0,len(etudList)):
                            print str(i + 1) + ". " + str(etudList[i])
                        e = raw_input("\nVotre choix:")
                        if validNumInput(e, etudList, optErr):
                            etudiant = etudList[int(e)-1]
                    else:
                        etudiant = etudList[0]            
                    print("\nEntrez les notes, -1 pour terminer")
                    n = raw_input("Note: ")
                    while n != "-1":
                        if validNote(n,0,20,numErr):
                            etudiant.ajoutNote(int(n))
                        n = raw_input("Note: ")
                    print "\nNotes ajoutees a l'etudiant : " + str(etudiant) 
                else: 
                    print "Etudiant inconnu"
            
            if choix == 5:
                nom = raw_input("Nom: ")
                prenom = raw_input("Prenom: ")
                age = raw_input("Age: ")
                if age.isdigit():       
                    etu = Etudiant(nom, prenom, age)
                    print "\nEntrez les notes, -1 pour terminer"
                    n = raw_input("Note: ")
                    while n != "-1":
                        if validNote(n, 0, 20, numErr):
                                etu.ajoutNote(int(n))
                        n = raw_input("Note: ")
                    etudiants.append(etu)
                    print "\nNouvel Etudiant : " + str(etu)
                else:
                    print optErr
         
  
        
    print "\n Application Moodle fermee"

elif name =='etu' and pwd =='etupass':
    print("Bienvenue dans l'application \"Moodle\"!")          
    while True:
        menuetu()

        choix = raw_input("Votre choix: ")
        if choix.lstrip('-').isdigit():
            choix = int(choix)
            optTitle(choix)

            if choix == 6:
                s = raw_input("Entrez votre nom: ")
                s = s.upper()
                idt = raw_input("Entrez votre identifiant: ")
                idt = idt.upper()
                m = raw_input("Entrez votre mot de passe: ")
                m = m.upper()
                etudList = []
                for etud in etudiants:
                    if s == etud.nom.upper()and idt == etud.ident.upper() and m == etud.mdp.upper():
                        etudList.append(etud)        
                if len(etudList) > 0:
                    etudiant = etudList[0]            
                    print "\nVoici vos notes: " + str(etudiant) 
                else: 
                    print "Etudiant inconnu"           
               
    print "\n Application Moodle fermee"
    
elif name =='prof' and pwd =='profpass':
    print("Bienvenue dans l'application \"Moodle\"!")          
    while True:
        menuprof()

        choix = raw_input("Votre choix: ")
        if choix.lstrip('-').isdigit():
            choix = int(choix)
            optTitle(choix)

            if choix == 2:
                nom = raw_input("Nom: ")
                matiere = Cours(nom)
                print "\nSelectionnez les etudiants, -1 pour terminer"
                for i in range(0, len(etudiants)):            
                    print str(i + 1) + ". " + str(etudiants[i])
                n = raw_input("\nVotre choix: ")
                while n != "-1":
                    if validNumInput(n, etudiants, optErr):
                        n = int(n)-1
                        if etudiants[n] not in matiere.etudiants:
                            matiere.ajoutEtud(etudiants[n])
                        else:
                            print "Deja present dans le cours"
                    n = raw_input("Votre choix: ")
                cours.append(matiere)
                print "\nNouveau Cours : " + str(matiere)

            if choix == 3:
                getCoursList()   
                n = raw_input("\nVotre choix: ")
                if validNumInput(n,  cours, optErr):
                    print "\n"+str(cours[int(n)-1])  
                
            if choix == 4:
                s = raw_input("Entrez le nom de l'etudiant: ")
                s = s.upper()
                etudList = []
                for etud in etudiants:
                    if s == etud.nom.upper():
                        etudList.append(etud)        
                if len(etudList) > 0:
                    if len(etudList) > 1:
                        print "\nSelectionnez l'etudiant:"
                        for i in range(0,len(etudList)):
                            print str(i + 1) + ". " + str(etudList[i])
                        e = raw_input("\nVotre choix:")
                        if validNumInput(e, etudList, optErr):
                            etudiant = etudList[int(e)-1]
                    else:
                        etudiant = etudList[0]            
                    print("\nEntrez les notes, -1 pour terminer")
                    n = raw_input("Note: ")
                    while n != "-1":
                        if validNote(n,0,20,numErr):
                            etudiant.ajoutNote(int(n))
                        n = raw_input("Note: ")
                    print "\nNotes ajoutees a l'etudiant : " + str(etudiant) 
                else: 
                    print "Etudiant inconnu"            
               
    print "\n Application Moodle fermee"
else :     
    print "L'identifiant et/ou le mot de passe sont incorrects" 

