from ProjetGestionEtudiants import Etudiant

class Cours:
    def __init__(self, nom):
        self.nom = nom
        self.etudiants = []
    def ajoutEtud(self, etud):
        self.etudiants.append(etud)
    def __repr__(self):
        res = self.nom + " - Etudiants : \n"
        for val in self.etudiants:
            res += str(val)
            if val != self.etudiants[-1]:
                res += "\n"
        return res 
