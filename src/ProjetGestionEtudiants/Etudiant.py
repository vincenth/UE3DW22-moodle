class Etudiant:
    def __init__(self, nom, prenom, ident, mdp):
        self.nom = nom
        self.prenom = prenom
        self.ident = ident
        self.mdp = mdp
        self.notes = []
    def ajoutNote(self, note):
        self.notes.append(note)
    def __repr__(self):
        res = self.prenom + " " + self.nom + " - Notes: "
        for val in self.notes:
            res += str(val) + " "
        return res 

